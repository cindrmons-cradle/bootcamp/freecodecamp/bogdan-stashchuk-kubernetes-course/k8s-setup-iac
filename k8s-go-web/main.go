package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

func main() {

	port := 4000

	r := chi.NewRouter()

	r.Use(middleware.Logger)
	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		hostname, err := os.Hostname()
		if err != nil {
			fmt.Fprintln(w, "os.Hostname can't be found...")
		}
		fmt.Fprintf(w, "VERSION 3: Hello from the %s\n", hostname)
	})

	log.Printf("Server now running at http://localhost:%d/\n", port)
	http.ListenAndServe(fmt.Sprintf(":%d", port), r)
}
