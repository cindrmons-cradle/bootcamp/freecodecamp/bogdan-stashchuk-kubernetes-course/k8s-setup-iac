# k8s Go Web Server

Taken from [https://gitlab.com/cindrmons-cradle/sandbox/go/go-api-docker-example](https://gitlab.com/cindrmons-cradle/sandbox/go/go-api-docker-example)

## Latest Image:

```
registry.gitlab.com/cindrmons-cradle/bootcamp/freecodecamp/bogdan-stashchuk-kubernetes-course/k8s-setup-iac:nginx-latest
```

## Container Registry

```
https://gitlab.com/cindrmons-cradle/bootcamp/freecodecamp/bogdan-stashchuk-kubernetes-course/k8s-setup-iac/container_registry
```
